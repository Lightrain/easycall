Git global setup:
    git config --global user.name "UserName"
    git config --global user.email "Email"
   
Create Repository
    mkdir easycall
    cd easycall
    git init
    touch README
    git add README
    git commit -m 'first commit'
    git remote add origin git@gitlab.com:Lightrain/easycall.git
    git push -u origin master
    
Existing Git Repo?
    cd existing_git_repo
    git remote add origin git@gitlab.com:Lightrain/easycall.git
    git push -u origin master


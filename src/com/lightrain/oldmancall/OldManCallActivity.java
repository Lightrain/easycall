package com.lightrain.oldmancall;

import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class OldManCallActivity extends Activity implements OnClickListener {
    private ContentResolver m_contentRes;
	private Cursor m_cr;
	private static float m_fTextSize = 40;
	private TextView m_tvContractName;
	private LinearLayout m_layoutShowPhoneList;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_old_man_call);
		
		initSystem();
		
		m_contentRes = this.getContentResolver();
		m_cr = m_contentRes.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		m_cr.moveToFirst();
		showContactInfo();
	}
	
	private void onDestory()
	{
		m_cr.close();
	}
	
	private void initSystem()
	{
		m_tvContractName = (TextView)this.findViewById(R.id.tvContractName);
		m_tvContractName.setTextSize(80);
		m_layoutShowPhoneList = (LinearLayout)this.findViewById(R.id.layoutShowPhoneList);
		
		Button btn = (Button)this.findViewById(R.id.btnPre);
		btn.setOnClickListener(this);
		btn.setTextSize(50);
		//btn.setBackgroundColor(Color.GREEN);
		btn = (Button)this.findViewById(R.id.btnNext);
		btn.setOnClickListener(this);
		btn.setTextSize(50);
		//btn.setBackgroundColor(Color.GREEN);
	}

	private void showNextContacts()
	{
		if (!m_cr.moveToNext())
		{
			m_cr.moveToFirst();
		}
		showContactInfo();
	}
	
	private void showPreContacts()
	{
		if (!m_cr.moveToPrevious())
		{
			m_cr.moveToLast();
		}
		showContactInfo();
	}
	
	private void showContactInfo()
	{
		int nameIndex = m_cr.getColumnIndex(PhoneLookup.DISPLAY_NAME);
		String strName = m_cr.getString(nameIndex);
		m_tvContractName.setText(strName);
		
		//// 打印全部列名
		/*
		for (int i = 0; i < cr.getColumnCount(); i++) 
	    {  
	        String columnName = cr.getColumnName(i);  
	        Log.v("LIGHTRAIN", "column name:" + columnName);  
	    }
	        */ 
		m_layoutShowPhoneList.removeAllViewsInLayout();
		//取得联系人的ID索引值
		String contactId = m_cr.getString(m_cr.getColumnIndex(ContactsContract.Contacts._ID)); 
		//查询该位联系人的电话号码，类似的可以查询email，photo
		Cursor phone = m_contentRes.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
				ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);//第一个参数是确定查询电话号，第三个参数是查询具体某个人的过滤值
		//一个人可能有几个号码
		while(phone.moveToNext())
		{
			String strPhoneNumber = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			Button btnTmp = new Button(this);
			btnTmp.setText(strPhoneNumber);
			btnTmp.setTextSize(m_fTextSize);
			btnTmp.setBackgroundColor(Color.RED);
			btnTmp.setOnClickListener(this);
			m_layoutShowPhoneList.addView(btnTmp);
			
			TextView tvTmpLine = new TextView(this);
			tvTmpLine.setText(" ");
			tvTmpLine.setTextSize(3);
			m_layoutShowPhoneList.addView(tvTmpLine);
		}
		phone.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.old_man_call, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		int viewId = v.getId();
		switch (viewId)
		{
		case R.id.btnPre:
			showPreContacts();
			break;
		case R.id.btnNext:
			showNextContacts();
			break;
		default:
			Button btn = (Button)v;
			String strNumber = btn.getText().toString();
			
			Intent phoneCallIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + strNumber));
			startActivity(phoneCallIntent);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		//按下键盘上返回按钮
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			this.onDestory();
			System.exit(0);
			return true;
		}
		else if (keyCode == KeyEvent.KEYCODE_MENU)
		{
			return true;
		}
		else
		{		
			return super.onKeyDown(keyCode, event);
		}
	}
}
